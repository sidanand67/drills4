const users = {
    John: {
        age: 24,
        designation: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland",
    },
    Ron: {
        age: 19,
        designation: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK",
    },
    Wanda: {
        age: 24,
        designation: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany",
    },
    Rob: {
        age: 34,
        designation: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA",
    },
    Pike: {
        age: 23,
        designation: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany",
    },
};


function videoGamesLover(){
    return Object.keys(users).reduce((acc, user) => {
        if(users[user].interests[0].search('Video Games') !== -1){
            acc.push(users[user]); 
        }
        return acc;
    }, []); 
}

function germanyUsers(){
    return Object.keys(users).filter(user => {
        if(users[user].nationality === "Germany"){
            return users[user]; 
        }
    }); 
}

function sortUsers(){
    let result =  Object.entries(users)
        .sort(([,a], [,b]) => {
            return b.age - a.age;
        })
        .sort(([, a], [, b]) => {
            if (
                (a.designation.includes("Senior") &&
                    b.designation.includes("Senior")) ||
                (a.designation.includes("Developer") &&
                    b.designation.includes("Developer")) ||
                (a.designation.includes("Intern") &&
                    b.designation.includes("Intern"))
            ) {
                return 0;
            } else if (
                (a.designation.includes("Senior") &&
                    !b.designation.includes("Senior")) ||
                (a.designation.includes("Developer") &&
                    b.designation.includes("Intern"))
            ) {
                return -1;
            } else {
                return 1;
            }
        }); 

    return Object.fromEntries(result); 
}

function mastersUsers(){
    return Object.keys(users).filter(user => {
        if(users[user].qualification === "Masters"){
            return users[user]; 
        }
    }); 
}

function groupUsersByLang(){
    let languages = ['javascript', 'golang', 'python']; 
    return languages.reduce((acc, curr) => {
        let filterUsers = Object.keys(users).filter(user => {
            if(users[user].designation.toLowerCase().includes(curr) !== false){
                return users[user]; 
            }
        }); 
        if(acc[curr]){
            acc[curr].push(filterUsers); 
        }
        else {
            acc[curr] = filterUsers; 
        }
        return acc; 
    }, {}); 
}

//  Function Calls
let videoGameLovers = videoGamesLover(); 
console.log(videoGameLovers); 

let germanyUsersList = germanyUsers(); 
console.log(germanyUsersList); 

let sortUsersList = sortUsers(); 
console.log(sortUsersList); 

let mastersUsersList = mastersUsers(); 
console.log(mastersUsersList); 

let groupUsersByLangList = groupUsersByLang(); 
console.log(groupUsersByLangList); 